
import json

from flask import Flask, request, jsonify
import awsgi
import boto3

try:
    import credentials
except ImportError:
    import example_credentials as credentials

app = Flask(__name__)


def check_credentials(token):
    return token == credentials.OUTGOING_WEBHOOK_TOKEN


def set_hacktory_status(open):
    s3 = boto3.resource('s3')
    s3.Object('hacktory', 'status.json').put(
        Body=json.dumps({'status': 'open' if open else 'closed'})
    )


def handle_open(text):
    words = text.lower().split(' ')
    if len(words) < 3:
        return
    words = words[1:]
    if words[0] != 'hacktory' and words[:2] != ['the', 'hacktory']:
        return
    if words[-1] == 'open':
        set_hacktory_status(True)
        return 'The Hacktory is open!'
    if words[-1] == 'closed':
        set_hacktory_status(False)
        return 'The Hacktory is closed!'


@app.route('/hackbot', methods=['POST'])
def hackbot():
    # check token
    if not check_credentials(request.form['token']):
        return jsonify(error='Not authorized')
    res = handle_open(request.form['text'])
    if res:
        return jsonify(text=res)
    return jsonify(submitted=request.form['text'])

def handle(event, context):
    return awsgi.response(app, event, context)
